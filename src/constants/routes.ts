export const ROUTES = {
  main: '/',
  search: '/search',
  about: '/about',
  contact: '/contact',
  terms: '/terms',
  privacy: '/privacy',
  notFound: '/404',
};
