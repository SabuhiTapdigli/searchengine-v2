// import { ChangeEvent } from 'react';
import { NavLink, useLocation } from 'react-router-dom';
// interface HeaderProps {
//   setMode: (mode: boolean) => void;
//   mode: boolean;
// }
const Header = () => {
  // const handleCheckboxChange = (event: ChangeEvent<HTMLInputElement>) => {
  //   setMode(!mode);
  // };
  const location = useLocation();
  const active = 'text-white md:text-white md:dark:text-white';
  return (
    <nav className="bg-sky-500 dark:bg-gray-900 w-full z-20 top-0 left-0  dark:border-gray-600">
      <div className=" max-w-screen-xl flex flex-wrap items-center justify-between mx-auto p-4">
        <NavLink to="/" className="flex items-center">
          <img src="/logo.svg" className="h-8 mr-3" alt="Search enginee Logo" />
        </NavLink>

        <div className="items-center justify-between hidden w-full md:flex md:w-auto md:order-1" id="navbar-sticky">
          <ul className="flex flex-col p-4 md:p-0 mt-4 font-medium border border-gray-100 rounded-lg bg-gray-50 md:flex-row md:space-x-8 md:mt-0 md:border-0 md:bg-sky-500 dark:bg-gray-800 md:dark:bg-gray-900 dark:border-gray-700">
            <li>
              <NavLink
                to="/"
                className={`block py-2 pl-3 pr-4  bg-white-700 rounded md:bg-transparent md:hover:text-white md:p-0 ${
                  location.pathname === '/' && active
                }`}
                aria-current="page"
              >
                Koti
              </NavLink>
            </li>
            <li>
              <NavLink
                to="/about"
                className={`block py-2 pl-3 pr-4 rounded hover:bg-gray-100 md:hover:bg-transparent md:hover:text-white md:p-0 md:dark:hover:text-blue-500 dark:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700 ${
                  location.pathname === '/about' && active
                }`}
              >
                Noin
              </NavLink>
            </li>
            <li>
              <NavLink
                to="/terms"
                className={`block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:hover:text-white md:p-0 md:dark:hover:text-blue-500 dark:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700 ${
                  location.pathname === '/terms' && active
                }  `}
              >
                Ehdot
              </NavLink>
            </li>
            <li>
              <NavLink
                to="/privacy"
                className={`block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:hover:text-white md:p-0 md:dark:hover:text-blue-500 dark:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700 ${
                  location.pathname === '/privacy' && active
                }`}
              >
                Yksityisyys
              </NavLink>
            </li>
            <li>
              <NavLink
                to="/contact"
                className={`block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:hover:text-white md:p-0 md:dark:hover:text-blue-500 dark:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700 ${
                  location.pathname === '/contact' && active
                }`}
              >
                Ottaa yhteyttä
              </NavLink>
            </li>
          </ul>
        </div>
        {/* <div className="flex md:order-2">
          <label className="relative inline-flex items-center cursor-pointer">
            <input type="checkbox" value="" className="sr-only peer" onChange={handleCheckboxChange} />
            <div className="w-11 h-6 bg-gray-200 peer-focus:outline-none peer-focus:ring-4 peer-focus:ring-blue-300 dark:peer-focus:ring-blue-800 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:left-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-5 after:w-5 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600"></div>
          </label>
        </div> */}
      </div>
    </nav>
  );
};

export default Header;
