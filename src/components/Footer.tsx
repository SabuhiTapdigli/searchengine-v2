import { NavLink } from 'react-router-dom';

const Footer = () => {
  return (
    <nav className="bg-sky-500 dark:bg-gray-900 w-full z-20 top-0 left-0  dark:border-gray-600">
      <div className="max-w-screen-xl flex flex-wrap items-center justify-between mx-auto p-4">
        <span className="block text-sm text-white sm:text-center dark:text-white">
          <NavLink to="/" className="hover:underline">
            <img src="/logo.svg" className="h-8 mr-3" alt="Search enginee Logo" />
          </NavLink>
        </span>

        <div className="items-center justify-between hidden w-full md:flex md:w-auto md:order-1" id="navbar-sticky">
          <span className="text-white">© 2023 . Kaikki oikeudet pidätetään.</span>
        </div>
      </div>
    </nav>
  );
};

export default Footer;
