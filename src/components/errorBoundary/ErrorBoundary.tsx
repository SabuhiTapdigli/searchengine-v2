import { Component } from 'react';

import ErrorMessage from './ErrorMessage';
import { ErrorBoundaryProps, ErrorBoundaryState } from '../../types';

export class ErrorBoundary extends Component<ErrorBoundaryProps, ErrorBoundaryState> {
  state = { hasError: false };

  componentDidCatch() {
    this.setState({ hasError: true });
  }
  render() {
    this.state.hasError && <ErrorMessage />;
    return this.props.children;
  }
}
