// import IconButton from '@mui/material/IconButton';
// import Tooltip from '@mui/material/Tooltip';
import { Avatar, Grid, IconButton, Tooltip, Typography } from '@mui/material';
import { useSelector } from 'react-redux/es/hooks/useSelector';
import { selectSearchTermList } from '../redux/reducers/searchTerm/selector';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { setSearchTerm } from '../redux/reducers/searchTerm';

const RecentSearch = () => {
  const { searchTermList } = useSelector(selectSearchTermList);
  console.log('searchTermList', searchTermList);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const recentSearchHandler = (e: React.MouseEvent<HTMLDivElement, MouseEvent>, item: string) => {
    e.preventDefault();
    dispatch(setSearchTerm(item));
    navigate('/search');
  };
  return (
    <div className="flex flex-col items-center justify-center max-w-sm m-auto">
      <Typography className="p-4">Recently Searched</Typography>
      <Grid container spacing={{ xs: 0 }} className="text-center">
        {searchTermList &&
          searchTermList.map((item) => (
            <Grid item xs={3} key={Math.random()}>
              <Tooltip title={item} onClick={(e) => recentSearchHandler(e, item)}>
                <IconButton>
                  <Avatar sx={{ width: 56, height: 56 }}>{item[0]}</Avatar>
                </IconButton>
              </Tooltip>
            </Grid>
          ))}
      </Grid>
    </div>
  );
};
export default RecentSearch;
