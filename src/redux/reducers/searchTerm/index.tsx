import { createSlice } from '@reduxjs/toolkit';
import { ISearchTerm } from '../../../types/reduxTypes';

const initialState: ISearchTerm = {
  searchTerm: '',
  searchTermList: [],
};
export const searchSlice = createSlice({
  name: 'searchTerm',
  initialState: initialState,
  reducers: {
    setSearchTerm: (state, action) => {
      state.searchTerm = action.payload;
      if (state.searchTermList.length >= 8) {
        state.searchTermList = [...state.searchTermList.slice(1), action.payload];
      } else if (action.payload) {
        state.searchTermList = [...state.searchTermList, action.payload];
      }
    },
  },
});

export const { setSearchTerm } = searchSlice.actions;
export default searchSlice.reducer;
