import { RootState } from '../../store';

const selectSearchTerm = (state: RootState) => state.search;
const selectSearchTermList = (state: RootState) => state.search;

export { selectSearchTerm, selectSearchTermList };
