import { useRoutes } from 'react-router-dom';
import Layout from '../layout/Layout';
import { ROUTES } from '../constants/routes';
import Main from '../page/Main';
import Search from '../page/Search';
import NotFound from '../page/NotFound';
import Contact from '../page/Contact';
import Terms from '../page/Terms';
import About from '../page/About';
import Privacy from '../page/Privacy';

const Routes = () => {
  return useRoutes([
    {
      element: <Layout />,
      children: [
        {
          path: ROUTES.main,
          element: <Main />,
        },
        { path: ROUTES.search, element: <Search /> },
        { path: ROUTES.contact, element: <Contact /> },
        { path: ROUTES.about, element: <About /> },
        { path: ROUTES.terms, element: <Terms /> },
        { path: ROUTES.privacy, element: <Privacy /> },
        {
          path: '*',
          element: <NotFound />,
        },
      ],
    },
  ]);
};

export default Routes;
