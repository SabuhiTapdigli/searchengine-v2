export interface ISearchTerm {
  searchTerm: string;
  searchTermList: string[];
}
