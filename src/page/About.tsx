const About = () => {
  return (
    <div className="2xl:container 2xl:mx-auto lg:py-16 lg:px-20 md:py-12 md:px-6 py-9 px-4 flex-1">
      <div className="flex flex-col lg:flex-row justify-between gap-8">
        <div className="w-full lg:w-5/12 flex flex-col justify-center">
          <h1 className="text-3xl lg:text-4xl font-bold leading-9 text-gray-800 pb-4">Meistä</h1>
          <p className="font-normal text-base leading-6 text-gray-600 ">
            Sosiaalisen kasinon pelaajille suunnatun hakukoneyrityksenä ymmärrämme toimittamisen tärkeyden tarkkoja ja
            osuvia tuloksia somekasinoihin liittyvissä kyselyissä. Tiimimme on optimoinut hakukoneemme varmistaa, että
            käyttäjät löytävät helposti etsimänsä tiedon, olivatpa he etsimässä vinkkejä ja strategioita heidän suosikki
            sosiaalisiin kasinopeleihinsä tai etsivät uusia alustoja kokeiltavaksi.
          </p>
          <br />
          <p>
            Hakukoneemme käyttää kehittyneitä algoritmeja sosiaalisten kasinoiden sisällön analysoimiseen ja
            luokitteluun sen perusteella tekijät, kuten osuvuus, suosio ja laatu. Otamme myös huomioon erityistarpeet ja
            sosiaalisten kasinopelaajien etuja, räätälöimme hakutuloksiamme tarjoamaan hyödyllisimpiä ja kiinnostavimpia
            sisältö mahdollista. Olitpa kokenut sosiaalisen kasinon pelaaja tai vasta aloittamassa, hakukoneemme on
            suunniteltu auttamaan sinua löytämään tiedot, joita tarvitset parantaaksesi pelikokemustasi. Olemme
            sitoutuneet tarjoaa parhaat mahdolliset hakutulokset sosiaalisiin kasinoihin liittyville kyselyille, jotta
            voit viettää vähemmän aikaa etsiminen ja enemmän aikaa pelata suosikkipelejäsi.
          </p>
        </div>
        <div className="w-full lg:w-8/12 ">
          <img className="w-full h-full" src="/img/about.jpeg" alt="A group of People" />
        </div>
      </div>
    </div>
  );
};

export default About;
