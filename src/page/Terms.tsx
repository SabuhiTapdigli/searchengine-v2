const Terms = () => {
  return (
    <div className="flex flex-col h-screen">
      <div className="flex-1 container mx-auto px-4 py-6">
        <h2 className="text-lg font-bold mb-4">Terms and Conditions</h2>
        <p className="mb-4">
          Tämä sopimusehtosopimus ("Sopimus") sisältää sovellettavat ehdot ja ehdot hakukonealustamme ("alusta")
          käyttöön. Avaamalla tai käyttämällä alustaa, sinä sitoutuu noudattamaan tämän sopimuksen ehtoja.
        </p>
        <p className="mb-4">
          Alusta on tarkoitettu henkilöiden käyttöön, jotka ovat vähintään 18-vuotiaita. Olet yksin vastuussa kaikkien
          sovellettavien lakien ja määräysten noudattamisesta käyttäessäsi alustaa.
        </p>
        <p className="mb-4">
          Kaikki alustan sisältö ja materiaalit, mukaan lukien teksti, grafiikka, logot, kuvat ja niihin rajoittumatta
          ohjelmistot, ovat yrityksemme tai lisenssinantajien omaisuutta ja ovat immateriaalioikeuslakien suojaamia. Et
          saa jäljentää, muokata, jakaa tai näyttää mitään sisältöä tai materiaalia alustalta ilman meidän etukäteen
          kirjallinen suostumus.
        </p>
        <p className="mb-4">
          Alusta tarjotaan "sellaisenaan"; ja &quot;kun saatavilla&quot; perusta. Teemme ei kaikenlaiset nimenomaiset
          tai epäsuorat esitykset tai takuut, jotka koskevat alustan toimintaa tai käyttöä, mukaan lukien, mutta ei
          rajoittuen, sisällön tarkkuus, täydellisyys, luotettavuus tai sopivuus tai materiaaleja alustalla.
          Kieltäydymme kaikista takuista, nimenomaisista tai oletetuista, mukaan lukien, mutta ei rajoittuen oletetut
          takuut kaupallisuudesta, sopivuudesta tiettyyn tarkoitukseen ja loukkaamattomuudesta.
        </p>
        <p className="mb-4">
          Emme ole missään tapauksessa vastuussa mistään vahingoista, mukaan lukien mutta ei rajoittuen suoriin,
          välillisiin, satunnaisiin, erityisistä tai välillisistä vahingoista, jotka johtuvat alustan käytöstä tai
          liittyvät siihen kyvyttömyys käyttää alustaa, vaikka meille on kerrottu tällaisten vahinkojen
          mahdollisuudesta.
        </p>
        <p className="mb-4">
          Sitoudut korvaamaan, puolustamaan ja pitämään meidät vaarattomina kaikista vaateista, vahingoista, velat,
          kulut ja kulut, mukaan lukien kohtuulliset asianajajat&apos; palkkiot, jotka johtuvat tai liittyvät alustan
          käytön tai tämän sopimuksen rikkomisen kanssa.
        </p>
        <p className="mb-4">
          Pidätämme oikeuden muuttaa tätä sopimusta milloin tahansa ilman ennakkoilmoitusta. Jatkuva käyttösi alustan
          tällaisten muutosten jälkeen hyväksyt tarkistetut käyttöehdot.
        </p>
        <p className="mb-4">
          Tähän Sopimukseen sovelletaan [lisää lainkäyttöalue] lakeja ja niitä tulkitaan niiden mukaisesti. Minkä
          tahansa tästä sopimuksesta johtuvat tai siihen liittyvät riidat ratkaistaan yksinomaan osavaltiossa tai
          liittovaltion tuomioistuimet, jotka sijaitsevat [lisää lainkäyttöalue].
        </p>
        <p className="mb-4">
          Tämä sopimus muodostaa koko sopimuksen sinun ja meidän välillämme liittyen alustan käyttöön ja korvaa kaikki
          aikaisemmat tai samanaikaiset viestit ja ehdotukset, olivatpa ne suullisia tai kirjallisia, teidän välillänne
          ja me.
        </p>
      </div>
    </div>
  );
};

export default Terms;
