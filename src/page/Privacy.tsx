const Privacy = () => {
  return (
    <div className="flex-1 container mx-auto px-4 py-6">
      <h2 className="text-lg font-bold mb-4">Privacy Policy</h2>
      <p className="mb-4">
        Tämä tietosuojakäytäntö ("käytäntö") selittää, kuinka keräämme, käytämme ja luovutamme sinua koskevia tietoja
        kun käytät hakukonealustamme ("alusta"). Käyttämällä alustaa hyväksyt tietojesi käsittely tämän käytännön
        mukaisesti.
      </p>
      <h3 className="text-md font-bold mb-2">Keräämämme tiedot</h3>
      <p className="mb-4">
        Saatamme kerätä tietoja sinusta, kun käytät alustaa, mukaan lukien mutta ei rajoittuen IP-osoitteesi,
        selaintyyppi, laitetyyppi ja hakukyselyt. Saatamme myös kerätä tietoja kolmansien osapuolien lähteistä, kuten
        sosiaalisen median alustat.
      </p>
      <h3 className="text-md font-bold mb-2">Kuinka käytämme tietoa</h3>
      <p className="mb-4">
        Saatamme käyttää keräämiämme tietoja tarjotaksemme ja parantaaksemme alustaa, mukauttaaksemme käyttökokemustasi
        kommunikoida kanssasi ja noudattaa lakisääteisiä velvoitteita.
      </p>
      <h3 className="text-md font-bold mb-2">Tiedon jakaminen</h3>
      <p className="mb-4">
        Saatamme jakaa tietosi kolmansien osapuolien palveluntarjoajien kanssa, jotka auttavat meitä käyttämään alustaa
        tytäryhtiöiden ja muiden alustan käyttäjien kanssa. Voimme myös jakaa tietosi lainvalvontaviranomaisille tai
        muut valtion virastot vastauksena haasteeseen, oikeuden määräykseen tai muuhun lailliseen pyyntöön.
      </p>
      <h3 className="text-md font-bold mb-2">Evästeet ja vastaavat tekniikat</h3>
      <p className="mb-4">
        Saatamme käyttää evästeitä ja vastaavia tekniikoita kerätäksemme tietoja alustan käytöstäsi henkilökohtaista
        sisältöä ja mainontaa sekä analysoida trendejä ja käyttötapoja. Saatat pystyä hallitsemaan evästeiden käyttöä
        selaimesi asetusten kautta.
      </p>
      <h3 className="text-md font-bold mb-2">Turvallisuus</h3>
      <p className="mb-4">
        Ryhdymme kohtuullisiin toimenpiteisiin tietojesi suojaamiseksi luvattomalta käytöltä, paljastamiselta tai
        käytöltä. Kuitenkin, mikään turvajärjestelmä ei ole täysin turvallinen, emmekä voi taata tietojesi
        turvallisuutta.
      </p>
      <h3 className="text-md font-bold mb-2">Muutokset tähän käytäntöön</h3>
      <p className="mb-4">
        Saatamme päivittää tätä käytäntöä ajoittain, ja ilmoitamme sinulle kaikista olennaisista muutoksista
        julkaisemalla uuden Käytäntö alustalla tai lähettämällä sinulle sähköpostin tai muun ilmoituksen. Jatka alustan
        käyttöä tällaisten muutosten jälkeen hyväksyt tarkistetun käytännön.
      </p>
      <h3 className="text-md font-bold mb-2">Ota meihin yhteyttä</h3>
      <p className="mb-4">
        Jos sinulla on kysyttävää tai huolenaiheita tästä käytännöstä, ota meihin yhteyttä osoitteessa [insert contact
        information].
      </p>
    </div>
  );
};

export default Privacy;
