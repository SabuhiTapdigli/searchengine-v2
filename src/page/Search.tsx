import { useSelector } from 'react-redux';
import ErrorMessage from '../components/Error';
import ResultList from '../components/ResultList';
import SearchInput from '../components/SearchInput';
import SideBar from '../components/SideBar';
import { selectSearchTerm } from '../redux/reducers/searchTerm/selector';
import { useGetSearchApiQuery } from '../services/search';
import { Spinner } from '../components/Spinner';

const Search = () => {
  const { searchTerm } = useSelector(selectSearchTerm);
  const { data, error, isLoading } = useGetSearchApiQuery({
    pageNumber: '1',
    pageSize: '10',
    searchText: searchTerm,
  });
  {
    error && <ErrorMessage error={error} />;
  }
  return (
    <div className="flex-grow">
      <SearchInput />
      <div className=" container mx-auto flex  p-4 justify-between items-start">
        {isLoading ? <Spinner /> : <ResultList data={data} />}
        <SideBar />
      </div>
    </div>
  );
};

export default Search;
