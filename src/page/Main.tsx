import { useEffect } from 'react';
import RecentSearch from '../components/RecentSearch';
import SearchInput from '../components/SearchInput';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { setSearchTerm } from '../redux/reducers/searchTerm';

const Main = () => {
  const navigate = useNavigate();
  const searchParams = new URLSearchParams(location.search);
  const keyword = searchParams.get('keyword');
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(setSearchTerm(keyword));
    if (keyword) navigate('/search');
  }, [keyword]);
  return (
    <div className="flex-grow grid items-center">
      <div>
        <SearchInput />
        <RecentSearch />
      </div>
    </div>
  );
};

export default Main;
