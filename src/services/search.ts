// Need to use the React-specific entry point to import createApi
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

const WEB_SEARCH_API_URL =
  "https://contextualwebsearch-websearch-v1.p.rapidapi.com/api/Search/WebSearchAPI";
const RAPID_API_KEY = "ec02907ad4mshccc96ea498df868p135deejsnc7d5a6add0b6";
const RAPID_API_HOST = "contextualwebsearch-websearch-v1.p.rapidapi.com";

// Define a service using a base URL and expected endpoints
export const searchApi = createApi({
  reducerPath: "searchApi",
  baseQuery: fetchBaseQuery({
    baseUrl: WEB_SEARCH_API_URL,
    prepareHeaders: (headers) => {
      headers.set("x-rapidapi-key", RAPID_API_KEY);
      headers.set("x-rapidapi-host", RAPID_API_HOST);
      return headers;
    },
  }),
  endpoints: (builder) => ({
    GetSearchApi: builder.query<
      any,
      { pageNumber: string; pageSize: string; searchText: string }
    >({
      query: ({ pageNumber, pageSize, searchText }) =>
        `?pageNumber=${pageNumber}&pageSize=${pageSize}&q=${encodeURIComponent(
          searchText
        )}`,
    }),
  }),
});

export const { useGetSearchApiQuery } = searchApi;
